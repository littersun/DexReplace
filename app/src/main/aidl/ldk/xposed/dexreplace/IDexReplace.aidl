// IDexReplace.aidl
package ldk.xposed.dexreplace;

// Declare any non-default types here with import statements
import ldk.xposed.dexreplace.ReplaceDexInfo;

interface IDexReplace {
    ReplaceDexInfo replaceDexInfo(String packageName, int pid);
    void updateReplaceDexInfo(String packageName, in ReplaceDexInfo dexReplaceInfo);
}

package ldk.xposed.dexreplace.ui

import android.graphics.drawable.Drawable

/**
 * Created by liangdengke on 2018/5/7.
 */
class AppInfo {
    var appName: String? = null
    var packagename: String? = null
    var appIcon: Drawable? = null
    var dexReplaceFiles: List<String>? = null
}
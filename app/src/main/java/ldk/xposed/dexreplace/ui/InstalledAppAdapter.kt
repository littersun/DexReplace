package ldk.xposed.dexreplace.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import ldk.xposed.dexreplace.R

/**
 * Created by liangdengke on 2018/5/7.
 */
class InstalledAppAdapter(val appInfoList: List<AppInfo>) : BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewHolder = if (convertView == null){
            val holder = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_installed, parent, false))
            holder
        }else{
            convertView.tag as ViewHolder
        }
        val appInfo = getItem(position)
        viewHolder.iconImage.setImageDrawable(appInfo.appIcon)
        viewHolder.appName.text = appInfo.appName
        viewHolder.dexText.text = appInfo.dexReplaceFiles?.joinToString(", ")?:""
        return viewHolder.rootView
    }

    override fun getItem(position: Int): AppInfo {
        return appInfoList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return appInfoList.size
    }

    private class ViewHolder(val rootView: View){
        val iconImage = rootView.findViewById(R.id.iv_icon) as ImageView
        val appName = rootView.findViewById(R.id.tv_app_name) as TextView
        val dexText = rootView.findViewById(R.id.tv_dex) as TextView

        init {
            rootView.tag = this
        }
    }
}
package ldk.xposed.dexreplace.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.BaseAdapter
import android.widget.TextView
import ldk.xposed.dexreplace.R

/**
 * Created by liangdengke on 2018/5/7.
 */
class DexFileAdapter(val dexFiles: List<String>): BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var viewHolder: DexViewHolder? = null
        val resultView =
        if (convertView == null){
            val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_dex_item, parent, false)
            viewHolder = DexViewHolder(view)
            view.tag = viewHolder
            view
        }else{
            viewHolder = convertView.tag as DexViewHolder?
            convertView
        }
        viewHolder!!.tvContent.text = getItem(position)
        return resultView
    }

    override fun getItem(position: Int): String {
        return dexFiles[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dexFiles.size
    }
}

class DexViewHolder(parentView: View){
    val tvContent = parentView.findViewById(R.id.tv_content) as TextView
}
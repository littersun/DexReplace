package ldk.xposed.dexreplace

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by liangdengke on 2018/4/25.
 */
class ReplaceDexInfo(val dexFilePath: String, val uuid: String) : Parcelable {

    var permissionStatus = STATUS_MANIFEST

    constructor(parcel: Parcel)
            : this(parcel.readString(), parcel.readString()){
        permissionStatus = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dexFilePath)
        parcel.writeString(uuid)
        parcel.writeInt(permissionStatus)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReplaceDexInfo> {
        val STATUS_NO_MANIFEST = -1
        val STATUS_MANIFEST = 0
        val STATUS_HAS_PERMISSION = 1

        override fun createFromParcel(parcel: Parcel): ReplaceDexInfo {
            return ReplaceDexInfo(parcel)
        }

        override fun newArray(size: Int): Array<ReplaceDexInfo?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return "ReplaceDexInfo(dexFilePath=$dexFilePath, uuid=$uuid);"
    }
}
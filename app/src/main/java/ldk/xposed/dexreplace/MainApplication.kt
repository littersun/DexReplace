package ldk.xposed.dexreplace

import android.app.Application
import android.util.Log

/**
 * Created by liangdengke on 2018/4/23.
 */
class MainApplication: Application() {
    companion object {
        private val TAG = "MainApplication";
    }


    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate");
    }
}
package ldk.xposed.dexreplace

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.IPackageManager
import android.content.pm.PackageManager
import android.icu.text.IDNA
import android.os.Build
import android.os.ServiceManager
import android.os.UserHandle
import android.text.TextUtils
import android.util.Log
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Created by ldk on 4/25/18.
 */
class DexReplaceService(private val mContext: Context): IDexReplace.Stub() {
    companion object {
        private val TAG = "DexReplaceService";
        private val DEBUG = true;

        // called by client
        fun service(): IDexReplace{
            return IDexReplace.Stub.asInterface(ServiceManager.getService("user.ldkdexreplace"))
        }

        fun register(classLoader: ClassLoader){
            val activityManagerServiceClazz = XposedHelpers.findClass("com.android.server.am.ActivityManagerService", classLoader)
            if (DEBUG){
                Log.d(TAG, "register: $classLoader")
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                XposedBridge.hookAllConstructors(activityManagerServiceClazz, object : XC_MethodHook(){
                    override fun afterHookedMethod(param: MethodHookParam) {
                        val context = XposedHelpers.getObjectField(param.thisObject, "mContext") as Context
                        register(classLoader, context, activityManagerServiceClazz)
                    }
                })
            }
        }

        private fun register(classLoader: ClassLoader, context: Context, activityManagerServiceClazz: Class<*>){
            if (DEBUG){
                Log.d(TAG, "register: $context")
            }
            val dexReplaceService = DexReplaceService(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                XposedHelpers.callStaticMethod(ServiceManager::class.java,
                        "addService", "user.ldkdexreplace", dexReplaceService, true)
            }

            XposedBridge.hookAllMethods(activityManagerServiceClazz, "systemReady", object : XC_MethodHook() {
                override fun afterHookedMethod(param: MethodHookParam?) {
                    dexReplaceService.onSystemReady()
                }
            })
        }
    }

    private val replaceDexInfoMap = ConcurrentHashMap<String, ReplaceDexInfo>()
    private lateinit var packageManager: IPackageManager

    override fun replaceDexInfo(packageName: String, pid: Int): ReplaceDexInfo? {
        if (DEBUG){
            Log.d(TAG, "replaceDexInfo($packageName) -> $pid")
        }
        if (replaceDexInfoMap.containsKey(packageName)){
            try {
                val packageInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS, UserHandle.USER_SYSTEM)
                var needPermission: String? = null
                packageInfo.requestedPermissions.forEach {
                    if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(it)){
                        needPermission = Manifest.permission.READ_EXTERNAL_STORAGE
                    }else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(it)){
                        needPermission = Manifest.permission.WRITE_EXTERNAL_STORAGE
                    }
                }

                val info = replaceDexInfoMap[packageName]
                if (needPermission == null
                        || packageManager.checkPermission(packageName, needPermission, UserHandle.USER_SYSTEM) != PackageManager.PERMISSION_GRANTED){
                    val intent = Intent("ldk.actions.grant")
                    intent.putExtra("packageName", packageName)
                    if (needPermission != null)
                        intent.putExtra("permission", needPermission)
                    intent.`package` = "ldk.xposed.dexreplace"
                    mContext.sendBroadcast(intent)
                    info?.permissionStatus = ReplaceDexInfo.STATUS_MANIFEST
                }
                return info
            }catch (e: Exception){
                Log.e(TAG, e.message, e)
            }
        }
        return null
    }

    override fun updateReplaceDexInfo(packageName: String, replaceDexInfo: ReplaceDexInfo?){
        if (DEBUG){
            Log.d(TAG, "updateReplaceDexInfo: $packageName ====> $replaceDexInfo")
        }
        if (replaceDexInfo == null){
            replaceDexInfoMap.remove(packageName)
        }else{
            replaceDexInfoMap[packageName] = replaceDexInfo
        }
        replaceDexInfoMap.forEach {
            XposedBridge.log("replaceDexInfoMap: key: ${it.key}, and value: ${it.value}")
        }
    }

    fun onSystemReady(){
        XposedBridge.log("onSystemReady")
        packageManager = IPackageManager.Stub.asInterface(ServiceManager.getService("package"))
    }
}
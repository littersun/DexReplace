package ldk.xposed.dexreplace

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log

/**
 * 用于DexReplaceReceiver发送消息给DexReplace应用
 * Created by liangdengke on 2018/5/7.
 */
class DexReplaceReceiver : BroadcastReceiver() {
    companion object {
        private val TAG = "DexReplaceReceiver"
    }

    override fun onReceive(context: Context, intent: Intent?) {
        val action = intent?.action
        Log.d(TAG, "onReceive: $action")
        if (action == null) return

        when(action){
            "ldk.actions.grant" -> {
                val packageName = intent.getStringExtra("packageName")
                val needPermission = intent.getStringExtra("permission")
                if (TextUtils.isEmpty(packageName)) return
                onGrantPermission(context, packageName, needPermission)
            }
        }
    }

    private fun onGrantPermission(context: Context, packageName: String, requestPermission: String?){
        Log.d(TAG, "onGrantPermission: $packageName")
    }
}
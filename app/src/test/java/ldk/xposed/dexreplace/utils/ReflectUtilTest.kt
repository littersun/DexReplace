package ldk.xposed.dexreplace.utils

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by liangdengke on 2018/4/26.
 */
class ReflectUtilTest{
    private val dexFile = "200"

    @Test
    fun findField(){
        val test = ReflectUtilTest()
        ReflectUtil.findField(test.javaClass, "dexFile")
    }

    @Test
    fun findFieldInner(){
        val element = Element()
        ReflectUtil.findField(element.javaClass, "dexFile")
    }

    class Element{
        private val dexFile = "30000"
    }
}
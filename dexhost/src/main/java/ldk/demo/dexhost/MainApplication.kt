package ldk.demo.dexhost

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import ldk.demo.dexplugin.DexPlugin
import java.io.File

/**
 * Created by liangdengke on 2018/5/7.
 */
class MainApplication : Application(){
    companion object {
        private val TAG = "MainApplication";
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate:")
        DexPlugin.out()
    }

    override fun attachBaseContext(base: Context) {
        val optFileDir = File(base.filesDir, "dexreplace")
        if (optFileDir.exists()){
            optFileDir.deleteRecursively()
        }
        optFileDir.mkdirs()

        val dexFile = File("/sdcard/3xingzhe/replace.dex")
/*        if (dexFile.exists()
                && dexFile.canRead()
                && base.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            Log.d(TAG, "开始替换: .....")
            ClassLoaderCompat.getInstance().install(base, listOf(dexFile), optFileDir)
        }*/
        super.attachBaseContext(base)
    }
}

package ldk.demo.dexhost

import android.app.Activity
import android.os.Bundle
import android.util.Log
import ldk.demo.dexplugin.DexPluginTwo

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onResume() {
        super.onResume()
        Log.d("Main",": onResume")
        Others.sayOther()
        DexPluginTwo.out()
    }
}
